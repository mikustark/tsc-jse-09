package ru.tsc.karbainova.tm.api;

public interface ICommandController {
    void displayWelcome();

    void exit();

    void showInfo();

    void displayHelp();

    void showCommands();

    void showArguments();

    void showCommandValue(String value);

    void displayVersion();

    void displayAbout();

    void displayErrorCommand();

    void displayErrorArg();
}
